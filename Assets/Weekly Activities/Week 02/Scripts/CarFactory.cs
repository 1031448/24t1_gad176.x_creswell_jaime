using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SAE.GAD176.Workshop.Week02.Encapsulation
{
    public class CarFactory : MonoBehaviour
    {
        [SerializeField] private Car carPreFab;

        private void Start()
        {
            Car newCar = Instantiate(carPreFab);

            newCar.SetModel("McLarren");

            Debug.Log(newCar.GetModel());
        }

    }
}

