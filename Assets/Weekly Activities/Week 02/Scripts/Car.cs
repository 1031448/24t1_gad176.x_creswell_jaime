using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SAE.GAD176.Workshop.Week02.Encapsulation
{
    public class Car : MonoBehaviour
    {
        private string model;
        private int year;
        // Start is called before the first frame update
        public void SetModel(string newModel)
        {
            model = newModel;
        }

        // Update is called once per frame
        public string GetModel()
        {
            return model;
        }

        public int GetYear()
        {
            return year;
        }
    }
}

