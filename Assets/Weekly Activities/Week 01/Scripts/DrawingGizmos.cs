using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SAE.GAD176.Workshop.Week01.Gizmos
{
    public class DrawingGizmos : MonoBehaviour
    {
        [SerializeField] private GameObject originObject;
        [SerializeField] private GameObject destinationObject;

        // Start is called before the first frame update
        void Update()
        {
            Vector3 origin = new Vector3(0, 0, 0);
            Vector3 Destination = new Vector3(1, 7, 12);

            // Draw a line for our vectors above
            Debug.DrawLine(origin, Destination, Color.yellow);

            // Draw a line bewtween our two game object references
            Debug.DrawLine(originObject.transform.position, destinationObject.transform.position, Color.blue);
        }
    }

}

